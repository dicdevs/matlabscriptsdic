function outputStruct = inputAppDialog(inputAppHandle,appInputs,appObjToBlock,blockingTitleStr,blockingMessageStr)
%INPUTAPPDIALOG Allows for an App Designer-based app to be called in a
%blocking manner and have it return outputs
% 
% Author: Adam Sifounakis
% Copyright 2018 The MathWorks, Inc.
% 
% Calling syntaxes:
% =================
% outputStruct = inputAppDialog(inputAppHandle)
%       This is the simplest way to call your app. It will launch your app
%       and wait until the user finishes making adjustments and exits the
%       app.
% 
%       Inputs:
%       =======
%       inputAppHandle: Function handle for your App Designer app
%                       Example: inputAppHandle = @YourAppName
% 
% outputStruct = inputAppDialog(inputAppHandle,appInputs)
%       If your app requires additional inputs, you can specify them using
%       the 2nd input argument.
% 
%       Inputs:
%       =======
%       appInputs:  Cell array containing each of the app's input arguments
%                   Example: appInputs = {1,"Hello World",pi}
% 
% outputStruct = inputAppDialog(inputAppHandle,appInputs,...
%                   appObjToBlockObj,blockingTitleStr,blockingMessageStr)
%       If you are calling your modal app from another App Designer app,
%       you can block the calling app by passing in the app object to be
%       blocked (appObjToBlock).
% 
%       Inputs:
%       =======
%       appObjToBlock:      App Designer app object or a UIFigure handle
% 	    blockingTitleStr:   String/char/cellstr containing the title of the
% 	                        blocking dialog
%       blockingMessageStr: String/char/cellstr containing the body message
%                           of the blocking dialog
% 
%       Notes:
%       ======
%           a) Non-scalar title and message inputs will be joined with new
%              line characters
%           b) If title or message inputs are not provided, default strings
%              will be used
% 
% 
% Goal:
% =====
% This code is designed to make it possible to create an App Designer-based
% blocking input dialog and have it return outputs (i.e. MATLAB will not go
% to the next line of code until the input dialog is finished).
% 
% This code is designed to hook into an App Designer app that you've built
% while only requiring four (4) 1-line code changes.
% 
% 
% Changes that need to be made to your app:
% =========================================
% These are the four (4) 1-line changes that need to be made to your app:
% 1.    Add an input argument to your app for the OutputAppCaller object
%       Note: This needs to be done with the "App Input Arguments" button
%       in the App Designer "Editor" Toolstrip.
% 
% 2.    Add a property in your app to store the OutputAppCaller object
%       input.
%       For example:
%           properties
%               OutputCallerObj
%           end
% 
% 3.    Add a line of code in your StartupFcn that saves the
%       OutputAppCaller object input to the property you created in step 2.
%       For example:
%           function StartupFcn(app,CallerObjInput)
%               app.OutputCallerObj = CallerObjInput;
%           end
% 
% 4.    Call the exportOutputs method just before you close your app.
%       For example:
%           function OkButtonPushed(app, event)
%               exportOutputs(app.CallerObj);
%               delete(app);
%           end


%% Error checking and input initialization

% Check number of arguments
narginchk(1,6);

% Initialize input arguments
if( nargin < 5 )
    blockingMessageStr = "This app is being blocked until the modal app has been closed.";
else
    validateattributes(blockingMessageStr,{'string','char','cellstr'},{},5);
    blockingMessageStr = join(string(blockingMessageStr),newline());
end

if( nargin < 4 )
    blockingTitleStr = "Waiting for modal dialog completion";
else
    validateattributes(blockingTitleStr,{'string','char','cellstr'},{},4);
    blockingTitleStr = join(string(blockingTitleStr),newline());
end

if( nargin < 3 )
    appObjToBlock = [];
else
    assertAppType(appObjToBlock,3);
end

if( nargin < 2 )
    appInputs = {};
else
    validateattributes(appInputs,{'cell','double'},{},2);
    if( class(appInputs) == "double" && ~isempty(appInputs) )
        % Convert to cell array for input argument expansion
        appInputs = {appInputs};
    end
end

validateattributes(inputAppHandle,{'function_handle'},{},1);


%% Call modal input app and get outputs
appCaller  = InputAppCaller(inputAppHandle,appInputs,...
    appObjToBlock,blockingTitleStr,blockingMessageStr);
outputStruct = getUserInput(appCaller);


end

function assertAppType(inputArg,inputArgInd)
%ASSERTAPPTYPE Verifies that the input argument is of an App type or
%UIFigure
classList = string([{class(inputArg)};superclasses(inputArg)]);
allowedAppTypes = ["matlab.ui.Figure";"matlab.apps.AppBase"];
if( ~any(ismember(classList,allowedAppTypes)) )
    errMessage = ["Expected input number " + inputArgInd + " to be one of these types:";
        "";
        allowedAppTypes;
        ""
        "Instead its type was " + string(class(inputArg)) + "."];
    mErr = MException('MATLAB:InputNotUIType',join(errMessage,newline()));
    throwAsCaller(mErr);
end

end

