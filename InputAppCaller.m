classdef InputAppCaller < handle
    %InputAppCALLER Class used to create an App Designer app-based input
    %dialog with captured outputs.
    %   Class used to create an App Designer app-based input dialog with
    %   captured outputs.
    % 
    % Author: Adam Sifounakis
    % Copyright 2018 The MathWorks, Inc.
    
    properties( Access = private )
        InputApp;
        InputAppFigure;
        InputAppHandle;
        InputAppInputs;
        InputAppOutputs;
        CallingAppToBlock;
        TitleStr;
        MessageStr;
        ProgressObj;
        CleanupObj;
    end
    
    methods
        function obj = InputAppCaller(inputAppHandle,inputAppInputs,callingAppObjToBlock,blockingTitleStr,blockingMessageStr)
            %InputAppCALLER Construct an instance of this class and save
            %user inputs
            %   Construct an instance of this class and save user inputs.
            
            % Set object properties
            obj.InputAppHandle    = inputAppHandle;
            obj.InputAppInputs    = inputAppInputs;
            obj.CallingAppToBlock = callingAppObjToBlock;
            obj.TitleStr          = blockingTitleStr;
            obj.MessageStr        = blockingMessageStr;
            
        end
        
        function outputStruct = getUserInput(obj)
            %GETUSERINPUT Launches input app and waits to capture outputs
            %   Launches input app, blocks input in calling app (if
            %   necessary), and waits for input app to complete before
            %   unblocking the calling app.
            
            % Block app if requested
            if( ~isempty(obj.CallingAppToBlock) )
                
                % Find calling app's figure
                if( class(obj.CallingAppToBlock) ~= "matlab.ui.Figure" )
                    fieldNames         = string(fields(obj.CallingAppToBlock));
                    classNames         = string(cellfun(@(fName) class(obj.CallingAppToBlock.(fName)),fieldNames,'UniformOutput',false));
                    figName            = fieldNames(classNames == 'matlab.ui.Figure');
                    obj.InputAppFigure = obj.CallingAppToBlock.(figName);
                else
                    obj.InputAppFigure = obj.CallingAppToBlock;
                end
                
                % Create blocking progress dialog on the calling app
                progressObj = uiprogressdlg(obj.InputAppFigure, ...
                    'Title'        , obj.TitleStr, ...
                    'Message'      , obj.MessageStr, ...
                    'Indeterminate', 'on');
                obj.ProgressObj = progressObj;
                
                % Set up cleanup object to close the blocking progress
                % dialog in the event of errors
                obj.CleanupObj = onCleanup(...
                    @() closeProgressDialog(obj,progressObj));
                
            end
            
            % Launch the input app
            obj.InputApp = obj.InputAppHandle(obj,obj.InputAppInputs{:});
            
            % Wait until input app is closed or cancelled
            waitfor(obj.InputApp);
            
            % Output captured outputs
            outputStruct = obj.InputAppOutputs;
            
        end
        
        function exportOutputs(obj)
            %EXPORTOUTPUTS Gets the value of all settable components
            %   Searches through an app for all user-settable components
            %   and properties and extracts their values
            
            % Component classes to extract
            validUIClasses = [...
                "matlab.ui.control.CheckBox";
                "matlab.ui.control.DatePicker";
                "matlab.ui.control.DropDown";
                "matlab.ui.control.EditField";
                "matlab.ui.control.ListBox";
                "matlab.ui.control.RadioButton";
                "matlab.ui.control.Slider";
                "matlab.ui.control.Spinner";
                "matlab.ui.control.StateButton";
                "matlab.ui.control.Table";
                "matlab.ui.control.TextArea";
                "matlab.ui.control.ToggleButton";
                "matlab.ui.control.NumericEditField";
                "matlab.ui.container.ButtonGroup"];
            
            % Find input objects
            fieldNames  = sort(string(fields(obj.InputApp)));
            classNames  = string(cellfun(@(fName) class(obj.InputApp.(fName)),fieldNames,'UniformOutput',false));
            uiObjsInd   = contains(classNames,"matlab.ui");
            nonInputInd = uiObjsInd & ~ismember(classNames,validUIClasses);
            
            % Remove all non-input fields
            fieldNames(nonInputInd) = [];
            classNames(nonInputInd) = [];
            uiObjsInd(nonInputInd)  = [];
            nInputObjs              = numel(fieldNames);
            
            % Create an array of the component objects
            inputObjs = cell(nInputObjs,1);
            for ii = 1:nInputObjs
                inputObjs{ii} = obj.InputApp.(fieldNames(ii));
            end
            
            % Find children in group objects
            childObjInd            = false(nInputObjs,1);
            groupObjsInd           = classNames == "matlab.ui.container.ButtonGroup";
            groupObjs              = vertcat(inputObjs{groupObjsInd});
            if( ~isempty(groupObjs) )
                childObjs              = vertcat(groupObjs.Children);
                childObjDupInd         = cellfun(@(obj) ismember(obj,childObjs),inputObjs(uiObjsInd));
                childObjInd(uiObjsInd) = childObjDupInd;
            end
            
            % Remove child objects from the list
            % Note: This removing child objects because the group objects
            % have a "SelectedObject" property that we will use
            fieldNames(childObjInd) = [];
            classNames(childObjInd) = [];
            inputObjs(childObjInd)  = [];
            uiObjsInd(childObjInd)  = []; %#ok<NASGU>
            nInputObjs              = numel(fieldNames);
            
            % Initialize output struct
            structFieldList = [cellstr(fieldNames)';cell(1,nInputObjs)];
            outputStruct = struct(structFieldList{:});
            
            % Extract data from each input object
            for ii = 1:nInputObjs
                switch classNames(ii)
                    case {"matlab.ui.control.CheckBox", ...
                            "matlab.ui.control.EditField", ...
                            "matlab.ui.control.NumericEditField", ...
                            "matlab.ui.control.DropDown", ...
                            "matlab.ui.control.ListBox", ...
                            "matlab.ui.control.Slider", ...
                            "matlab.ui.control.Spinner", ...
                            "matlab.ui.control.StateButton", ...
                            "matlab.ui.control.TextArea", ...
                            "matlab.ui.control.DatePicker"}
                        outputStruct.(fieldNames(ii)) = inputObjs{ii}.Value;
                        
                    case "matlab.ui.container.ButtonGroup"
                        outputStruct.(fieldNames(ii)) = vertcat(inputObjs{ii}.SelectedObject.Text);
                        
                    case "matlab.ui.control.Table"
                        outputStruct.(fieldNames(ii)) = inputObjs{ii}.Data;
                        
                    otherwise
                        outputStruct.(fieldNames(ii)) = inputObjs{ii};
                        
                end
                
                % Save captured outputs to obj for later output
                obj.InputAppOutputs = outputStruct;
                
            end
            
        end
        
        function closeProgressDialog(~,progObj)
            %CLOSEPROGRESSDIALOG Closes the progress dialog if it is still
            %open when "obj" is deleted
            %   Closes the progress dialog and deletes the cancel button
            %   listener if they still exist still open when "obj" is
            %   deleted
            
            % Delete/close progress dialog
            if( isvalid(progObj) )
                delete(progObj);
            end
        end
        
    end
    
end

